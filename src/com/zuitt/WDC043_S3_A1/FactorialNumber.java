package com.zuitt.WDC043_S3_A1;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args){
        System.out.println("Input an integer whose factorial will be computed");

        Scanner in = new Scanner(System.in);

        int num = 0;
        int answer = 1;
        int counter = 1;

        try {
            num = in.nextInt();

            if (num < 0) {
                System.out.println("Please Enter a positive number.");
            } else if (num == 0){
                System.out.println("The factorial of "+num+" is 1");
            } else {
                while(counter <= num){
                    answer *= counter;
                    counter++;
                }
                System.out.println("While: The factorial of "+num+" is "+answer);

                answer = 1;

                for(counter = 1; counter<=num; counter++){
                    answer *= counter;
                }
                System.out.println("For: The factorial of "+num+" is "+answer);
            }
        }
        catch (Exception e) {
            System.out.println("Invalid input. Please enter a positive number.");

            e.printStackTrace();

        }
    }
}
